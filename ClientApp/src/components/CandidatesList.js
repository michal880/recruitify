import React, { Component } from 'react';
import { getCandidates } from '../candidates'; 
import { AddNewCandidate } from './AddNewCandidate';
import { EditDecision } from './EditDecision';
export class CandidatesList extends Component {
  static displayName = CandidatesList.name;

  constructor(props) {
    super(props);

    this.state = { candidates: [], loading: true, addCandidateModalOpened: false, editCandidateModalOpened: false, editedCandidate: null};
  } 

  async componentDidMount() {
  
    const candidates = await getCandidates();
    this.setState({ candidates: candidates, loading: false });
  }

  renderCandidatesTable() {
    
    return (
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Condidate name</th>
            <th>Decision</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {this.state.candidates.map(candidate =>
            <tr key={candidate.candidateName}>
              <td>{candidate.candidateName}</td>
                <td>{CandidatesList.getDisplayedDecision(candidate.decision)}</td>
                <td><button className="btn btn-primary float-right" onClick={() => this.toggleEditCandidateModal(candidate)}>Change decision</button></td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : this.renderCandidatesTable();
    return (
      <div>
        <h1 id="tabelLabel" >Candidates <button className="btn btn-primary float-right" onClick={this.toggleAddCandidateModal.bind(this)}>Add new candidate</button></h1>
        {contents}
        {this.state.addCandidateModalOpened ?  
        this.renderAddNewCandidateModal()  : null  }  
         {this.state.editCandidateModalOpened ?  
        this.renderEditCandidateModal(this.state.editedCandidate)  : null  }  
      </div>
    );
    
  }
  toggleAddCandidateModal() {  
    this.setState({  
      addCandidateModalOpened: !this.state.addCandidateModalOpened  
    });
  }
  toggleEditCandidateModal(candidate) {  
    this.setState({  
      editCandidateModalOpened: !this.state.editCandidateModalOpened,
      editedCandidate: candidate
    });
  }
   renderAddNewCandidateModal(){
     return (
       <AddNewCandidate toggleModal={this.toggleAddCandidateModal.bind(this)} handleSubmitCandidate={this.handleSubmitCandidate.bind(this)}></AddNewCandidate>
    );
  }
  renderEditCandidateModal(candidate){
    console.log(candidate);
    return (
      <EditDecision editedCandidate={candidate} originalDecision={this.state} toggleModal={this.toggleEditCandidateModal.bind(this)} handleDecisionUpdate={this.handleDecisionUpdate.bind(this)}/>
    );
  }
  handleSubmitCandidate(newCandidate){
    this.state.candidates.push(newCandidate)
    this.toggleAddCandidateModal();
  }
  handleDecisionUpdate(updatedCandidate){
    const newCandidatesState = [...this.state.candidates];
    const candidateIndex = this.state.candidates.findIndex(candidate => candidate.candidateName === updatedCandidate.candidateName);
    newCandidatesState[candidateIndex].decision =  updatedCandidate.decision;
    this.setState({candidates: newCandidatesState});
    this.toggleEditCandidateModal();
  }
  static getDisplayedDecision(decision){
    switch(decision) {
      case "accepted":
        return "Accepted"
      case "rejected":
        return "Rejected"
      case "notDecided":
        return "Not decided"
      default:
        return "Not decided"
    }
  }
  
}
