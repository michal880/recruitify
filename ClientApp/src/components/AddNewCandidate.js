import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, Col } from 'reactstrap';
export class AddNewCandidate extends Component {
  constructor() {
    super();
    this.state = { candidateName: "", decision: "notDecided" };
    this.handleChange = this.handleChange.bind(this);
  }

  render() {
    return (
      <Modal isOpen={true} >
        <ModalHeader >Add a new candidate</ModalHeader>
        <ModalBody>
          <Form >
            <FormGroup row>
              <Label for="candidateName" sm={5}>Candidate name</Label>
              <Col sm={5}>
                <Input type="text" name="candidateName" id="candidateName" placeholder="Emhyr"
                  onChange={this.handleChange} />
              </Col>
              <Label for="decision" sm={5}>Decision</Label>
              <Col sm={5}>
                <select name="decision" onChange={this.handleChange} value={this.state.decision}>
                  <option value="notDecided">Not decided yet</option>
                  <option value="rejected">Rejected</option>
                  <option value="accepted">Accepted</option>
                </select>
              </Col>
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.handleSubmit.bind(this)}>Save</Button>
          <Button color="danger" onClick={this.props.toggleModal}>Cancel</Button>
        </ModalFooter>
      </Modal>
    );
  }
  handleChange(event) {
    event.preventDefault();
    const value = event.target.value;
    this.setState({ ...this.state, [event.target.name]: value });
  }
  handleSubmit(){
    const newCandidate = { ...this.state };
    this.props.handleSubmitCandidate(newCandidate);
  }
}