import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, Col } from 'reactstrap';

export class EditDecision extends Component {
    constructor(props) {
        super();
        this.state = { newDecsion: "notDecided" };

    }
    render() {

        return (
            <Modal isOpen={true} >
                <ModalHeader >Change decision for {this.props.editedCandidate.candidateName}</ModalHeader>
                <ModalBody>
                    <Form >
                        <FormGroup row>
                            <Label for="decision" sm={5}>Decision</Label>
                            <Col sm={5}>
                                <select name="decision" onChange={this.handleChange.bind(this)} defaultValue={this.props.originalDecision}>
                                    <option value="notDecided">Not decided yet</option>
                                    <option value="rejected">Rejected</option>
                                    <option value="accepted">Accepted</option>
                                </select>
                            </Col>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.handleUpdate.bind(this)}>Save</Button>
                    <Button color="danger" onClick={this.props.toggleModal}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    };
    handleChange(event) {
        event.preventDefault();
        this.setState({ newDecsion: event.target.value });
      }
    handleUpdate() {
        const newCandidate = { candidateName: this.props.editedCandidate.candidateName, decision : this.state.newDecsion };
        console.log(newCandidate)
        this.props.handleDecisionUpdate(newCandidate);
    }
}
