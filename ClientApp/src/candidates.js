export const getCandidates = async () => { 
    const response = 
    [
        {
          "candidateName": "Eustachy",
          "decision": "notDecided"
        },
        {
          "candidateName": "Ezekiel",
          "decision": "rejected"
        },
        {
          "candidateName": "Euzebiusz",
          "decision": "notDecided"
        },
        {
          "candidateName": "Edgar",
          "decision": "notDecided"
        },
        {
          "candidateName": "Eneasz",
          "decision": "accepted"
        }
      ];
    return response; 
  }