﻿using Microsoft.AspNetCore.Mvc;
using Recruitify.Application;


namespace Recruitify.API.Controllers
{
    [Route("api/[controller]")]
    public class CandidatesController : ControllerBase
    {
        private readonly CandidateService _candidateService;

        public CandidatesController(CandidateService candidateService)
        {
            _candidateService = candidateService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_candidateService.GetAllCandidates());
        }

        [HttpGet("{candidateName}")]
        public IActionResult Get([FromRoute]string candidateName)
        {
            return Ok(_candidateService.GetCandidate(candidateName));
        }

        [HttpPost]
        public IActionResult Post([FromBody]CandidateDto candidateDto)
        {
            _candidateService.AddCandidate(candidateDto);
            return Created($"candidates/{candidateDto.Name}", candidateDto.Name);
        }

        [HttpPut("{candidateName}/decision")]
        public IActionResult Put([FromRoute]string candidateName, [FromBody]string newDecision)
        {
            _candidateService.ChangeCandidateDecision(candidateName, newDecision);
            return Ok();
        }

    }
}
