﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Recruitify.Application;
using Recruitify.Infrastructure;
using System;
using Microsoft.EntityFrameworkCore;

namespace Recruitify.API.Tests
{
    public class TestWebApplicationFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var serviceProvider = new ServiceCollection()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();

                services.AddDbContext<RecruitifyContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryDbForTesting");
                    options.UseInternalServiceProvider(serviceProvider);
                });
                services.AddScoped<IUnitOfWork, EfCoreUnitOfWork>();
                
                var sp = services.BuildServiceProvider();
                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<RecruitifyContext>();

                    var logger = scopedServices
                        .GetRequiredService<ILogger<TestWebApplicationFactory>>();
                    try
                    {
                        db.Database.EnsureCreated();

                        var unitOfWork = scopedServices
                            .GetRequiredService<IUnitOfWork>();
                        InMemoryDbSeeder.InitializeForTests(db, unitOfWork);
                    }
                    catch (Exception ex)
                    {
                        logger.LogWarning("An issue occured during attempt to set-up in memory database for testing",
                            ex);
                    }
                }
            });
        }
    }
}
