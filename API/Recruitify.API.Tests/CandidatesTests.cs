using Recruitify.Application;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Recruitify.API.Tests
{
    public class CandidatesTests : IClassFixture<TestWebApplicationFactory>
    {
        private readonly HttpClient _client;

        public CandidatesTests(TestWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
        }
        [Fact]
        public async Task ShouldReturnListOfCandidates()
        {
            //Arrange
            var endpoint = "/api/candidates";
            var minimalExpectedCount = 3;
            //Act
            var response = await _client.GetAsync(endpoint);
            var contentTask =  response.Content.ReadAsAsync<IEnumerable<CandidateDto>>();

            //Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccessStatusCode);
            var content = await contentTask;
            Assert.NotNull(content);
            Assert.True(minimalExpectedCount <= content.Count());
        }

        [Fact]
        public async Task ShouldSuccessfullyAddCandidate()
        {
            //Arrange
            var endpoint = "/api/candidates";
            var payload = new CandidateDto {Name = "Emhyr", Decision = "accepted"};

            //Act
            var response = await _client.PostAsJsonAsync(endpoint, payload);
            
            //Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccessStatusCode);
        }

        [Fact]
        public async Task ShouldSuccessfullyAddCandidateAndThenRetrieve()
        {
            //Arrange
            var endpoint = "/api/candidates";
            var payload = new CandidateDto { Name = "Eames", Decision = "accepted" };

            //Act
            await _client.PostAsJsonAsync(endpoint, payload);
            var response = await _client.GetAsync($"{endpoint}/{payload.Name}");
            var content = await response.Content.ReadAsAsync<CandidateDto>();

            //Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccessStatusCode);
            Assert.Equal(payload.Name, content.Name);
            Assert.Equal(payload.Decision, content.Decision);
        }

        [Fact]
        public async Task ShouldChangeExistingCandidateDecision()
        {
            //Arrange
            var endpoint = "/api/candidates";
            var candidateName = "Eneasz";
            var newCandidateDecision = "accepted";

            //Act
            await _client.PutAsJsonAsync($"{endpoint}/{candidateName}/decision", newCandidateDecision);
            var response = await _client.GetAsync($"{endpoint}/{candidateName}");
            var content = await response.Content.ReadAsAsync<CandidateDto>();

            //Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccessStatusCode);
            Assert.Equal(candidateName, content.Name);
            Assert.Equal(newCandidateDecision, content.Decision);
        }
    }
}
