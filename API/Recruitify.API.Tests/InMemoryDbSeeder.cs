﻿using Recruitify.Application;
using Recruitify.Domain.Candidate;
using Recruitify.Infrastructure;
using System.Collections.Generic;

namespace Recruitify.API.Tests
{
    public class InMemoryDbSeeder
    {
        public static void InitializeForTests(RecruitifyContext db, IUnitOfWork unitOfWork)
        {
            db.Candidates.AddRange(GetSeedCandidates);
            unitOfWork.Commit();
        }

        private static IEnumerable<Candidate> GetSeedCandidates => new List<Candidate>
        {
            new Candidate("Eneasz"),
            new Candidate("Ernest", Decision.Accepted),
            new Candidate("Eugeniusz", Decision.Rejected),
        };

    }
}