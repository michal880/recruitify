using Microsoft.EntityFrameworkCore;
using Recruitify.Domain.Candidate;

namespace Recruitify.Infrastructure
{
    public class RecruitifyContext : DbContext
    {
        public RecruitifyContext(DbContextOptions<RecruitifyContext> options)
            : base(options)
        {
        }
        public DbSet<Candidate> Candidates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Candidate>().HasKey(x => x.Name);
        }
    }
}