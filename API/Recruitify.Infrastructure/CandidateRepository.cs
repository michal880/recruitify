﻿using Recruitify.Domain.Candidate;
using System.Collections.Generic;
using System.Linq;

namespace Recruitify.Infrastructure
{
    public class CandidateRepository : ICandidateRepository
    {
        private readonly RecruitifyContext _context;

        public CandidateRepository(RecruitifyContext context)
        {
            _context = context;
        }
        public IEnumerable<Candidate> GetAll()
        {
            return _context.Candidates.AsEnumerable();
        }
        public Candidate Get(string candidateName)
        {
            return _context.Candidates.Single(q => q.Name.Equals(candidateName));
        }

        public void Add(Candidate candidate)
        {
            _context.Candidates.Add(candidate);
        }

        public void Update(Candidate candidate)
        {
            _context.Candidates.Update(candidate);
        }
    }
}
