﻿using Recruitify.Application;

namespace Recruitify.Infrastructure
{
    public class EfCoreUnitOfWork : IUnitOfWork
    {
        private readonly RecruitifyContext _dbContext;

        public EfCoreUnitOfWork(RecruitifyContext dbContext)
            => _dbContext = dbContext;
        public void Commit() => _dbContext.SaveChanges();
    }
}
