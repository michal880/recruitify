﻿namespace Recruitify.Domain.Candidate
{
    public enum Decision
    {
        NotDecided,
        Accepted,
        Rejected
    }
}
