﻿using System;

namespace Recruitify.Domain.Candidate
{
    public class Candidate
    {
        private Candidate(){}
        public Candidate(string name, Decision decision = Decision.NotDecided)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Name cannot be null or empty");
            Name = name;
            Decision = decision;
        }

        public string Name { get; private set; }
        public Decision Decision { get; private set; }

        public void ChangeDecision(Decision newDecision)
        {
            Decision = newDecision;
        }
    }
}
