﻿using System.Collections.Generic;

namespace Recruitify.Domain.Candidate
{
    public interface ICandidateRepository
    {
        IEnumerable<Candidate> GetAll();
        Candidate Get(string candidateName);
        void Add(Candidate candidate);
        void Update(Candidate candidate);
    }
}
