using System;
using Recruitify.Domain.Candidate;
using Xunit;

namespace Recruitify.Domain.Tests
{
    public class CandidateTests
    {
        [Fact]
        public void CreatingCandidateWithEmptyNameThrowsEx()
        {
            // Arrange

            void ThrowingMethod()
            {
                var entity = new Candidate.Candidate("");
            }
            
            // Act 
            
            // Assert
            Assert.Throws<ArgumentException>(ThrowingMethod);
        }
        [Fact]
        public void CreatingCandidateWithoutDecisionResultsInNotDecidedDecision()
        {
            // Arrange
            var expectedDecision = Decision.NotDecided;

            // Act 
            var candidate = new Candidate.Candidate("Emhyr");

            // Assert
            Assert.Equal(expectedDecision, candidate.Decision);
        }
    }
}
