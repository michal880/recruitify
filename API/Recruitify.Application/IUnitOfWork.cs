﻿namespace Recruitify.Application
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}