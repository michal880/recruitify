﻿namespace Recruitify.Application
{
    public class CandidateDto
    {
        public string Name { get; set; }
        public string Decision { get; set; }
    }
}