﻿using System;
using Recruitify.Domain.Candidate;
using System.Collections.Generic;

namespace Recruitify.Application
{
    public class CandidateService
    {
        private readonly ICandidateRepository _candidateRepository;
        private readonly IUnitOfWork _unitOfWork;
        public CandidateService(ICandidateRepository candidateRepository, IUnitOfWork unitOfWork)
        {
            _candidateRepository = candidateRepository;
            _unitOfWork = unitOfWork;
        }

        public void AddCandidate(CandidateDto candidateDto)
        {
            var candidate = new Candidate(candidateDto.Name, (Decision)Enum.Parse(typeof(Decision),  candidateDto.Decision,true));
            _candidateRepository.Add(candidate);
            _unitOfWork.Commit();
        }

        public IEnumerable<CandidateDto> GetAllCandidates()
        {
            var candidates = _candidateRepository.GetAll();
            foreach (var candidate in candidates)
            {
                yield return MapCandidateEntityToDto(candidate);
            }
        }

        public CandidateDto GetCandidate(string candidateName)
        {
            return MapCandidateEntityToDto(_candidateRepository.Get(candidateName));
        }

        public void ChangeCandidateDecision(string candidateName, string newDecision)
        {
            var candidate = _candidateRepository.Get(candidateName);
            candidate.ChangeDecision((Decision)Enum.Parse(typeof(Decision), newDecision, true));
            _candidateRepository.Update(candidate);
            _unitOfWork.Commit();
        }

        private static CandidateDto MapCandidateEntityToDto(Candidate candidateEntity)
        {
            return new CandidateDto {Name = candidateEntity.Name, Decision = candidateEntity.Decision.ToString().ToLower()};
        }
    }
}
